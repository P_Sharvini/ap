

public interface Booking {

	public void start();
	public void bookSeat();
	public void firstClassBooking();
	public void economyClassBooking();
}
